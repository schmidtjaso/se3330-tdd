using NUnit.Framework;
using System;
using SIS;

namespace TestSIS
{
    public class TestStudent
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestIsMinor_True()
        {
            Student test_student = new Student();
            test_student.birth_date = new DateTime(2006, 4, 1);
            Assert.AreEqual(true, test_student.IsMinor(), "IsMinor: false negative");
        }

        [Test]
        public void TestIsMinor_False()
        {
            Student test_student = new Student();
            test_student.birth_date = new DateTime(2000, 4, 1);
            Assert.AreEqual(false, test_student.IsMinor(), "IsMinor: false positive");
        }

        [Test]
        public void TestIsMinor_Boundary()
        {
            Student test_student = new Student();
            test_student.birth_date = new DateTime(2005, 2, 14);
            Assert.AreEqual(false, test_student.IsMinor(), "IsMinor: boundary test failed");
        }
               
    }
}